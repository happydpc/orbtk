pub static DEFAULT_THEME_CSS: &'static str = include_str!("dark.css");
pub static LIGHT_THEME_EXTENSION_CSS: &'static str = include_str!("light.css");
