use crate::prelude::*;

property!(
    /// `IconSize` describes the icon size of a text element.
    IconSize(f64)
);
