use crate::prelude::*;

property!(
    /// `Opacity` describes the opacity of a widget.
    Opacity(f64)
);
