use crate::prelude::*;

property!(
    /// `IconSize` describes the degree to which the corners of a Border are rounded.
    BorderRadius(f64)
);
